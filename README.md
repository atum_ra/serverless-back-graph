
## Starting the local server

```
npm install
npm run start
```

This should start local DynamoDB emulator and Serverless offline. You can now make API calls against `http://localhost:3000/api` like this:

```
curl http://localhost:3000/api/graph

Serverless: GET /api/graph (λ: getGraph)
Serverless: The first request might take a few extra seconds
Serverless: [200] {"statusCode":200,"headers":{"Access-Control-Allow-Origin":"*","Access-Control-Allow-Credentials":true},"body":"{\"graph\":[]}"}
```

## Running tests locally
```
npm test
```
![Architecture Diagram](tests.png)

# How it works

## Overview
This repo uses [Serverless Framework](https://serverless.com) to describe, test and deploy the start version of back-end for Charging bull broker software(CBBS) project to [AWS Lambda](https://aws.amazon.com/lambda/). AWS Lambda provides "serverless" cloud functions as a service. [AWS API Gateway](https://aws.amazon.com/api-gateway/) is used to expose the deployed Lambda functions as a HTTP REST API.

## API
The API is described in the [`serverless.yml`](serverless.yml) file. For example the following snippet instructs AWS Lambda to execute the `create` method in [`src/User.js`](src/User.js) whenever a `POST` method is called on `/api/users`:
```
functions:

  ## Users API
  createUser:
    handler: src/User.create
    events:
      - http:
          method: POST
          path: /api/users
          cors: true

  ...
```

## Storage
For storage, [AWS DynamoDB](https://aws.amazon.com/dynamodb/) a managed serverless NoSQL database is used. Tables are created to store `users`, `graph_data` also described in `serverless.yml` file. For example:
```
resources:
  Resources:

    UsersDynamoDBTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        AttributeDefinitions:
        ...
```

## Deployment
To deploy the code to AWS, simply execute:
```
npm run deploy
```
This will use `serverless` to deploy the API as described in `serverless.yml`.

Once deployed, you can test the deployed API by executing:
```
npm run test:deployed
```
