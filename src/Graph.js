const Util = require('./Util');
const graphTable = Util.getTableName('graph');
const User = require('./User');

/**
 * @module Graph
 */
module.exports = {

  /** Get graph */
  async get(event) {
    
    const authenticatedUser = await User.authenticateAndGetUser(event);

    let graph = (await Util.DocumentClient.query({
      TableName: graphTable,
      KeyConditions: {
        'date': {
          ComparisonOperator: 'BEGINS_WITH',
          AttributeValueList: ['2018']
        },
        'dummy': {
          ComparisonOperator: 'EQ',
          AttributeValueList: ['ok']
        }
      },
      Select: 'ALL_ATTRIBUTES',
      Limit: 25,
      ScanIndexForward: false
    }).promise().catch(e => e));

    if (!graph.Items ) {
      return Util.envelop(`No data found or error appeared`, 422);
    }      
    graph = graph.Items.slice().sort((a, b) => {
      if (a.date > b.date) {
        return 1;
      }
      if (a.date < b.date) {
        return -1;
      }
        return 0;
    });

    if (graph && !authenticatedUser) {
      graph.splice(-2, 2);
     
    }

    return Util.envelop({
      graph: graph
    });
  }
};


