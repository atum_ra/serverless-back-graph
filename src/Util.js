/* istanbul ignore next */
if (!process.env.AWS_REGION) {
  process.env.AWS_REGION = 'us-east-1';
}

/* istanbul ignore next */
if (!process.env.DYNAMODB_NAMESPACE) {
  process.env.DYNAMODB_NAMESPACE = 'dev';
}

const AWS = require('aws-sdk');

// In offline mode, use DynamoDB local server
let DocumentClient = null;
/* istanbul ignore next */
if (process.env.IS_OFFLINE) {
  AWS.config.update({
    region: 'localhost',
    endpoint: "http://localhost:8000"
  });
}
DocumentClient = new AWS.DynamoDB.DocumentClient();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
  async ping() {
    await batchedAsync({
      list: graphData.map(el => {
        return {
          dummy: "ok",
          date: Object.keys(el)[0],
          quotas: el[Object.keys(el)[0]]
        };
      }),
      callback: writeItems,
      chunkSize: 25, // adjust to provisioned throughput. Max 25 (batchWrite dynamodb limit)
      msDelayBetweenChunks: 1000,
      TableName: getTableName('graph')
    });

     const user = {
      username: 'test',
      email: 'test@test.test',
      password: 'test'
    };

    const encryptedPassword = bcrypt.hashSync(user.password, 5);
    await DocumentClient.put({
      TableName: getTableName('users'),
      Item: {
        username: user.username,
        email: user.email,
        password: encryptedPassword,
      },
    }).promise();

    return envelop({
      token: mintToken(user.username),
      pong: new Date(),
      AWS_REGION: process.env.AWS_REGION,
      DYNAMODB_NAMESPACE: process.env.DYNAMODB_NAMESPACE,
    });
  },

  async purgeData() {
    await purgeTable('users', 'username');
    await purgeTable('graph', 'date');
   // await purgeTable('comments', 'id');
    return envelop('Purged all data!');
  },

  getTableName,

  envelop,

  tokenSecret: /* istanbul ignore next */ process.env.SECRET ?
    process.env.SECRET : '3ee058420bc2',
  
  DocumentClient

};

function envelop(res, statusCode = 200) {
  let body;
  if (statusCode === 200) {
    body = JSON.stringify(res, null, 2);
  } else {
    body = JSON.stringify({ errors: { body: [res] } }, null, 2);
  }
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    body,
  };
}

function getTableName(aName) {
  return `serverless-show-chart-cbbs-${process.env.DYNAMODB_NAMESPACE}-${aName}`;
}

async function purgeTable(aTable, aKeyName) /* istanbul ignore next */ {
  const tableName = module.exports.getTableName(aTable);

  if (!tableName.includes('dev') && !tableName.includes('test')) {
    console.log(`WARNING: Table name [${tableName}] ` +
      `contains neither dev nor test, not purging`);
    return;
  }

  const allRecords = await DocumentClient
    .scan({ TableName: tableName }).promise();
  const deletePromises = [];
  for (let i = 0; i < allRecords.Items.length; ++i) {
    const recordToDelete = {
      TableName: tableName,
      Key: {},
    };
    recordToDelete.Key[aKeyName] = allRecords.Items[i][aKeyName];
    deletePromises.push(DocumentClient.delete(recordToDelete).promise());
  }
  await Promise.all(deletePromises);
}

function mintToken(aUsername) {
  return jwt.sign({ username: aUsername },
    '3ee058420bc2', { expiresIn: '2 days' });
}

const graphData = [
  {
    "2018-08-13": {
      "hold": [
        { "ADM": [50.06, 49.32, -1.48] },
        { "AES": [13.82, 13.38, -3.18] },
        { "BHGE": [33.94, 34.12, 0.53] },
        { "FCX": [15.4, 14.92, -3.12] },
        { "HPQ": [23.74, 23.8, 0.25] },
        { "LYB": [113.01, 111.56, -1.27] },
        { "M": [38.78, 40.08, 3.35] },
        { "NRG": [33.27, 32.5, -2.31] },
        { "UAA": [19.71, 20.8, 5.53] }
      ]
      ,
      "sell": [
        { "LB": [32.23, 31.24, -3.07] }
      ]
      ,
      "buy": [
        { "HRB": [25.71, 0, 0] }
      ]
      ,
      "S&P 500": [0, 0, -1.0]
    }
  },
  {
    "2018-08-06": {
      "hold": [
        { "ADM": [47.35, 50.06, 5.72] },
        { "AES": [13.3, 13.82, 3.91] },
        { "FCX": [16.08, 15.40, -4.23] },
        { "HPQ": [23.1, 23.74, 2.77] },
        { "LB": [31.04, 32.23, 3.83] },
        { "LYB": [109.11, 113.01, 3.57] },
        { "M": [40.27, 38.78, -3.70] },
        { "NRG": [33.27, 32.5, -2.31] },
        { "UAA": [20.259, 19.710, -2.71] }
      ]
      ,
      "sell": [
        { "IPG": [32.23, 31.24, -3.07] },
        { "WU": [32.23, 31.24, -3.07] }
      ]
      ,
      "buy": [
        { "BHGE": [33.94, 0, 0] },
        { "NRG": [33.27, 0, 0] }
      ]
      ,
      "S&P 500": [0, 0, 1.71]
    }
  },
  {
    "2018-07-30": {
      "hold": [
        {"ADM": [47.1, 47.35, 0.53]},
        {"AES": [13.63, 13.3, 5.3]},
        {"FCX": [18.11, 16.08, -11.21]},
        {"HPQ": [23.37, 23.1, -1.16]},
        {"IPG": [ 23.09, 22.22, -3.77]},
        {"LB": [37.41, 31.04, -17.03]},
        {"LYB": [110.153, 109.11, -1.28]},
        {"M": [36.46, 40.27, 10.45]},
        {"WU": [20.3, 20.11, -0.94]}
      ],
      "sell": [
        {"HPE": [15.23, 15.59, 2.36]}
      ],
      "buy": [
        {"UAA": [20.259, 0, 0]}
      ],
      "S&P 500": [0, 0, 0.66
      ]
    }
  }
];

async function batchedAsync(
  { list, callback, chunkSize = 10, msDelayBetweenChunks = 0, TableName }) {
  const emptyList = new Array(Math.ceil(list.length / chunkSize)).fill();
  const clonedList = list.slice(0);
  const chunks = emptyList.map(() => clonedList.splice(0, chunkSize));
  for (const chunk of chunks) {
    if (msDelayBetweenChunks) {
      await new Promise(resolve => setTimeout(resolve, msDelayBetweenChunks));
    }
    await callback(chunk, chunks, TableName);
  }
}

async function writeItems(chunk, chunks, TableName) {
  const data = await DocumentClient.batchWrite({
    RequestItems: {
      [TableName]: chunk.map(item => {
        return {PutRequest: {Item: item}};
      })
    }
  }).promise().catch(e=> e);
  if (data.UnprocessedItems && data.UnprocessedItems.length) {
    chunks.push(data.UnprocessedItems);
  }
}



