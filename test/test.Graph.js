const TestUtil = require('./TestUtil');
const assert = require('assert');
const axios = require('axios');

const globals = {
  authorUser: null,
};

describe('Graph', async () => {

  before(async () => {
    globals.authUser = await TestUtil.createTestUser(
      `author-${TestUtil.randomString()}`);

    await axios.get(`/ping/`);
  });

  describe('Get', async () => {

    it('should get article by authorized user', async () => {
      const graph = await axios.get(`/graph/`, {
          headers: {
            Authorization: `Bearer ${globals.authUser.token}`
          },
        });

        
        assert((graph.data.graph[2].date) === '2018-08-13',
          `Expected last graph date with last date`);
    });

    it('should get article by unauthorized user', async () => {
      const graph = await axios.get(`/graph/`, {
          headers: {
            Authorization: `Bearer 000`
          },
        });
        assert((graph.data.graph[0].date) === '2018-07-30',
          `Expected last graph date is 2 dates before`);
    });

  });

});
