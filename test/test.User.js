const TestUtil = require('./TestUtil');
const assert = require('assert');
const axios = require('axios');

const username = `user1-${Math.random().toString(36)}`;
const userToCreate = {
  email: `${username}@email.com`,
  username: username,
  password: 'password',
};
let loggedInUser = null;

describe('User', async () => {

  describe('Create', async () => {

    it('should create user', async () => {
      const createdUser = (await axios.post(
        `/users`, { user: userToCreate })).data.user;
      assertUserEquals(createdUser, userToCreate);
    });

    it('should disallow same username', async () => {
      await axios.post(
        `/users`, { user: userToCreate }).catch(res => {
        assert.equal(res.response.status, 422);
        assert(/Username already taken/.test(res.response.data.errors.body[0]));
      });
    });

    it('should disallow same email', async () => {
      await axios.post(`/users`, {
        user: {
          username: 'user2',
          email: userToCreate.email,
          password: 'password',
        }
      }).catch(res => {
        assert.equal(res.response.status, 422);
        assert(/Email already taken/.test(res.response.data.errors.body[0]));
      });
    });

    it('should enforce required fields', async () => {
      await axios.post(`/users`, {})
        .catch(res => {
          TestUtil.assertError(res, /User must be specified/);
        });
      await axios.post(`/users`, {
        user: { foo: 1 }
      }).catch(res => {
        TestUtil.assertError(res, /Username must be specified/);
      });
      await axios.post(`/users`, {
        user: { username: 1 }
      }).catch(res => {
        TestUtil.assertError(res, /Email must be specified/);
      });
      await axios.post(`/users`, {
        user: { username: 1, email: 2 }
      }).catch(res => {
        TestUtil.assertError(res, /Password must be specified/);
      });
    });

  });

  describe('Login', async () => {

    it('should login', async () => {
      loggedInUser = (await axios.post(`/users/login`, {
        user: { email: userToCreate.email, password: userToCreate.password }
      })).data.user;
      assertUserEquals(loggedInUser, userToCreate);
    });

    it('should disallow unknown email', async () => {
      await axios.post(`/users/login`, {
        user: { email: Math.random().toString(36), password: 'somepassword' }
      }).catch(res => {
        TestUtil.assertError(res, /Email not found/);
      });
    });

    it('should disallow wrong password', async () => {
      await axios.post(`/users/login`, {
        user: {
          email: userToCreate.email,
          password: Math.random().toString(36)
        }
      }).catch(res => {
        TestUtil.assertError(res, /Wrong password/);
      });
    });

    it('should enforce required fields', async () => {
      await axios.post(`/users/login`, {}).catch(res => {
        TestUtil.assertError(res, /User must be specified/);
      });
      await axios.post(`/users/login`, { user: {} }).catch(res => {
        TestUtil.assertError(res, /Email must be specified/);
      });
      await axios.post(`/users/login`, {
        user: { email: 'someemail' }
      }).catch(res => {
        TestUtil.assertError(res, /Password must be specified/);
      });
    });

  });

  describe('Get', async () => {

    it('should disallow bad tokens', async () => {
      await axios.get(`/user`)
        .catch(res =>
          TestUtil.assertError(res, /Token not present or invalid/));
      await axios.get(`/user`, { headers: { foo: 'bar' } })
        .catch(res =>
          TestUtil.assertError(res, /Token not present or invalid/));
      await axios.get(`/user`, { headers: { Authorization: 'foo' } })
        .catch(res =>
          TestUtil.assertError(res, /Token not present or invalid/));
      await axios.get(`/user`, {
        headers: { Authorization: 'Token: foo' }
      }).catch(res =>
        TestUtil.assertError(res, /Token not present or invalid/));
    });

  });

  describe('Profile', async () => {

    it('should disallow unknown username', async () => {
      await axios.get(`/profiles/foo_${Math.random().toString(36)}`)
        .catch(res => { TestUtil.assertError(res, /User not found/); });
    });
  });

  describe('Update', async () => {
    // let loggedInUser = (await axios.post(`/users/login`, {
    //   user: { email: userToCreate.email, password: userToCreate.password }
    // })).data.user;

    before(async () => {
      loggedInUser = (await axios.post(`/users/login`, {
        user: { email: userToCreate.email, password: userToCreate.password }
      })).data.user;

      
  
    });

    

    it('should update user', async () => {
      
      const userMutations = [{
        email: `updated-${username}@email.com`,
      }, {
        password: 'newpassword',
      }, {
        bio: 'newbio',
      }, {
        image: 'newimage',
      }, ];

      for (let i = 0; i < userMutations.length; ++i) {
        const userMutation = userMutations[i];
        const updatedUser = (await axios.put('/user', {
          user: userMutation,
        }, {
          headers: { 'Authorization': `Bearer ${loggedInUser.token}` },
        })).data.user;

        if (!userMutation.password) {
          const field = Object.keys(userMutation)[0];
          assert.equal(updatedUser[field], userMutation[field]);
        }
        (updatedUser); // TODO: Assert on updatedUser
      }
    });

    it('should disallow missing token/email in update', async () => {
      await axios.put(`/user`).catch(res => {
        TestUtil.assertError(res, /Token not present or invalid/);
      });
      await axios.put(`/user`, {}, {
        headers: { 'Authorization': `Bearer ${loggedInUser.token}` },
      }).catch(res => {
        TestUtil.assertError(res, /User must be specified/);
      });
    });

    it('should disallow reusing email', async () => {
      const newUser = await TestUtil.createTestUser(
        `user2-${Math.random().toString(36)}`);
      await axios.put(`/user`, {
        user: { email: newUser.email },
      }, {
        headers: { 'Authorization': `Bearer ${newUser.token}` },
      }).catch(res => {
        TestUtil.assertError(res, /Email already taken/);
      });

    });

  });

});

function assertUserEquals(actual, expected) {
  assert.equal(actual.username, expected.username);
  assert.equal(actual.email, expected.email);
  assert.equal(typeof actual.token, 'string');
  assert.equal(actual.bio, expected.bio || '');
  assert.equal(actual.image, expected.image || '');
}
